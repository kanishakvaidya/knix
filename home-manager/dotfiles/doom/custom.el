(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(custom-safe-themes
   '("7fd8b914e340283c189980cd1883dbdef67080ad1a3a9cc3df864ca53bdc89cf" "835868dcd17131ba8b9619d14c67c127aa18b90a82438c8613586331129dda63" default)))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(markdown-header-face ((t (:inherit font-lock-function-name-face :weight bold :family "Source Code Pro"))))
 '(markdown-header-face-1 ((t (:inherit org-level-1 :height 1.5 :overline t))))
 '(markdown-header-face-2 ((t (:inherit org-level-2 :height 1.4 :overline t))))
 '(markdown-header-face-3 ((t (:inherit org-level-3 :height 1.3 :overline t))))
 '(markdown-header-face-4 ((t (:inherit org-level-4 :height 1.2))))
 '(markdown-header-face-5 ((t (:inherit org-level-5 :height 1.1))))
 '(markdown-header-face-6 ((t (:inherit org-level-6 :height 1.1)))))
