#+title: Doom emacs config
#+PROPERTY: header-args :tangle config.el
#+STARTUP: showeverything
#+OPTIONS: toc:2

* Table of Content :TOC:
- [[#identification][Identification]]
- [[#look-and-feel][Look and Feel]]
  - [[#fonts][Fonts]]
  - [[#theme-and-line-number][Theme and Line Number]]
- [[#org-mode][Org Mode]]
  - [[#look-and-feel-1][Look and Feel]]
  - [[#org-publish][Org Publish]]
  - [[#language-support][Language Support]]
  - [[#org-roam][Org roam]]
- [[#package-configuration-notes][Package configuration notes]]
- [[#spelling-and-grammar][Spelling and Grammar]]
- [[#company-mode][Company mode]]
- [[#latex-configuration][Latex configuration]]
  - [[#basic-theming][Basic theming]]
  - [[#latex-mode-keybindings][LaTeX-mode keybindings]]
  - [[#company-backends][Company backends]]
- [[#dired][Dired]]
- [[#elfeed][Elfeed]]
  - [[#my-elfeed-org-directory][My elfeed-org directory]]
  - [[#elfeed-only-show-feeds-from-last-one-week][Elfeed only show feeds from last one week]]
  - [[#automatic-feed-update][Automatic feed update]]

* Identification
Some functionality uses this to identify you, e.g. GPG configuration, email clients, file templates and snippets. It is optional.
#+begin_src emacs-lisp
(setq user-full-name "Kanishak Vaidya"
      user-mail-address "kanishak@gmail")
#+end_src

* Look and Feel
** Fonts
Doom exposes five (optional) variables for controlling fonts in Doom:

- `doom-font' -- the primary font to use
- `doom-variable-pitch-font' -- a non-monospace font (where applicable)
- `doom-big-font' -- used for `doom-big-font-mode'; use this for presentations or streaming.
- `doom-unicode-font' -- for unicode glyphs
- `doom-serif-font' -- for the `fixed-pitch-serif' face
#+begin_src emacs-lisp
(set-face-attribute 'default nil
  :font "JetBrains Mono Nerd Font"
  :height 150
  :weight 'medium)
(set-face-attribute 'variable-pitch nil
  :font "JetBrains Mono Nerd Font"
  :height 150
  :weight 'medium)
(set-face-attribute 'fixed-pitch nil
  :font "JetBrains Mono Nerd Font"
  :height 150
  :weight 'medium)
;; Makes commented text and keywords italics.
;; This is working in emacsclient but not emacs.
;; Your font must have an italic face available.
(set-face-attribute 'font-lock-comment-face nil
  :slant 'italic)
(set-face-attribute 'font-lock-keyword-face nil
  :slant 'italic)

;; This sets the default font on all graphical frames created after restarting Emacs.
;; Does the same thing as 'set-face-attribute default' above, but emacsclient fonts
;; are not right unless I also add this method of setting the default font.
;; (add-to-list 'default-frame-alist '(font . "JetBrains Mono Nerd Font-14"))

;; Uncomment the following line if line spacing needs adjusting.
(setq-default line-spacing 0.14)

;; (setq doom-font (font-spec :family "JetBrains Mono Nerd Font" :size 20 :weight 'Regular)
;;       doom-variable-pitch-font (font-spec :family "Sans" :size 13))
#+end_src

*** Custom font sizes for Markdown mode
Using org header properties for markdown headers.
#+begin_src emacs-lisp
(custom-set-faces
 '(markdown-header-face ((t (:inherit font-lock-function-name-face :weight bold :family "Source Code Pro"))))
 '(markdown-header-face-1 ((t (:inherit org-level-1 :height 1.5 :overline t))))
 '(markdown-header-face-2 ((t (:inherit org-level-2 :height 1.4 :overline t))))
 '(markdown-header-face-3 ((t (:inherit org-level-3 :height 1.3 :overline t))))
 '(markdown-header-face-4 ((t (:inherit org-level-4 :height 1.2))))
 '(markdown-header-face-5 ((t (:inherit org-level-5 :height 1.1))))
 '(markdown-header-face-6 ((t (:inherit org-level-6 :height 1.1)))))
#+end_src

** Theme and Line Number
#+begin_src emacs-lisp
(add-to-list 'custom-theme-load-path "~/.config/doom/themes/")
(setq doom-theme 'doom-tomorrow-night)
;; (setq doom-theme 'everforest-hard-dark)
(setq display-line-numbers-type t)
(setq fancy-splash-image "~/.config/doom/images/levitating-gnu.png")
;; (setq fancy-splash-image "~/.config/doom/images/legendaryPokes.png")
(setq +doom-dashboard-banner-padding '(2 . 2))
(setq +doom-dashboard-functions
  '(doom-dashboard-widget-banner
    doom-dashboard-widget-shortmenu
    ;; doom-dashboard-widget-loaded
    ;; doom-dashboard-widget-footer
    ))
(vertico-reverse-mode)
(setq vertico-resize t
      vertico-cycle t)
#+end_src
* Org Mode
** Look and Feel
Org mode should use bullets instead of asteriks and org mode should open with all heading folded
#+begin_src emacs-lisp
(require 'org-bullets)
(add-hook 'org-mode-hook (lambda () (org-bullets-mode 1)))
(setq org-startup-folded t)
#+end_src

org-edit-src-code should open a new window
#+begin_src emacs-lisp
;; Using RETURN to follow links in Org/Evil
;; Unmap keys in 'evil-maps if not done, (setq org-return-follows-link t) will not work
(setq org-src-window-setup 'other-frame)
(with-eval-after-load 'evil-maps
  (define-key evil-motion-state-map (kbd "SPC") nil)
  (define-key evil-motion-state-map (kbd "RET") nil)
  (define-key evil-motion-state-map (kbd "TAB") nil))
;; Setting RETURN key in org-mode to follow links
  (setq org-return-follows-link  t)
#+end_src
** Org Publish
#+begin_src emacs-lisp
(setq org-html-validation-link nil
      org-export-with-broken-links t
      org-html-head-include-scripts nil
      org-html-head-include-default-style nil)

(setq org-publish-project-alist
      '(("kvos-site"
         :base-directory "~/doc/repos/arch-kvos/org-site"
         :base-extension "org"
         :publishing-directory "~/doc/repos/arch-kvos/docs"
         :publishing-function org-html-publish-to-html
         :recursive t
         :with-author nil           ;; Don't include author name
         :with-creator t            ;; Include Emacs and Org versions in footer
         :with-toc nil              ;; Include a table of contents
         :section-numbers nil       ;; Don't include section numbers
         :time-stamp-file nil)
        ("kvos-static"
         :base-directory "~/doc/repos/arch-kvos/org-site"
         :base-extension "css\\|js\\|png\\|jpg\\|gif\\|pdf\\|mp3\\|ogg\\|swf\\|sh\\|md"
         :publishing-directory "~/doc/repos/arch-kvos/docs"
         :recursive t
         :publishing-function org-publish-attachment)
        ("kvos" :components ("kvos-site" "kvos-static"))
        ("phd-progress"
         :base-directory "~/doc/repos/profile"
         :base-extension "org"
         :publishing-directory "~/doc/repos/profile/docs"
         :recursive t
         :with-author nil           ;; Don't include author name
         :with-creator nil          ;; Include Emacs and Org versions in footer
         :with-toc nil              ;; Include a table of contents
         :section-numbers nil       ;; Don't include section numbers
         :publishing-function org-html-publish-to-html)))
#+end_src
** Language Support
#+begin_src emacs-lisp
(org-babel-do-load-languages
 'org-babel-load-languages
 '((octave . t)
   (C . t)))
#+end_src
** Org roam
#+begin_src emacs-lisp
(setq org-directory "~/doc/notes/org/"
      org-roam-directory "~/doc/notes/org/roam")
#+end_src
* Package configuration notes
Whenever you reconfigure a package, make sure to wrap your config in an `after!' block, otherwise Doom's defaults may override your settings. E.g.

  (after! PACKAGE
    (setq x y))

The exceptions to this rule:

  - Setting file/directory variables (like `org-directory')
  - Setting variables which explicitly tell you to set them before their
    package is loaded (see 'C-h v VARIABLE' to look up their documentation).
  - Setting doom variables (which start with 'doom-' or '+').

Here are some additional functions/macros that will help you configure Doom.

- `load!' for loading external *.el files relative to this one
- `use-package!' for configuring packages
- `after!' for running code after a package has loaded
- `add-load-path!' for adding directories to the `load-path', relative to this file. Emacs searches the `load-path' when you load packages with `require' or `use-package'.
- `map!' for binding new keys

To get information about any of these functions/macros, move the cursor over the highlighted symbol at press 'K' (non-evil users must press 'C-c c k'). This will open documentation for it, including demos of how they are used. Alternatively, use `C-h o' to look up a symbol (functions, variables, faces, etc).

You can also try 'gd' (or 'C-c c d') to jump to their definition and see how they are implemented.

* Spelling and Grammar
Using spell-fu instead of flyspell as spell-fu is faster.
#+begin_src emacs-lisp
(after! spell-fu
  (setq ispell-dictionary "en_US"))
#+end_src
* Company mode
Change minimum prefix length for company completion to be 1 letter
#+begin_src emacs-lisp
(after! company-mode
  (setq company-minimum-prefix-length 2
        company-reftex-annotate-citations t))
#+end_src
* Latex configuration
** Basic theming
I don't want superscript or subscript and preview PDF in zathura
#+begin_src emacs-lisp
(setq tex-fontify-script nil)
(setq font-latex-fontify-script nil)
(setq +latex-viewers '(zathura))
(setq-default TeX-master nil)
#+end_src
** LaTeX-mode keybindings
#+begin_src emacs-lisp :tangle nil
(map! :map LaTeX-mode-map
      :leader
      (:prefix ("l" . "LaTeX Option")
      :desc "View PDF" "v" #'TeX-view
      :desc "Compile PDF" "r" #'TeX-command-master "LaTeX" 'TeX-master-file
      :desc "Show TOC" "=" #'reftex-toc
      :desc "reftex-citations" "[" #'reftex-citation
      :desc "reftex-labels" "(" #'reftex-label))
#+end_src
** Company backends
As there is [[https://github.com/doomemacs/doomemacs/issues/5672][some issue]] with ~set-company-backend!~ so [[https://docs.doomemacs.org/latest/modules/completion/company/#enable-company-backends-certain-modes,code-1][this method]] of setting company backends don't work.
#+begin_src emacs-lisp :tangle nil
(after! latex
  (set-company-backend! 'latex-mode nil)
  (set-company-backend! 'latex-mode '(company-files company-dabbrev :with company-capf company-yasnippet)
    'company-reftex-labels 'company-reftex-citations
    '(:separate company-dabbrev company-yasnippet company-ispell)))
#+end_src

Therefore, I am using [[https://github.com/doomemacs/doomemacs/issues/5672#issuecomment-949466410][this meantime fix]] for setting ~company-backend~ in LaTeX-mode.
#+begin_src emacs-lisp
(setq-hook! 'LaTeX-mode-hook +lsp-company-backends '(:separate company-capf company-yasnippet
 company-reftex-labels company-reftex-citations company-files company-dabbrev company-dabbrev-code)) 
#+end_src

* Dired
#+begin_src emacs-lisp
(evil-define-key 'normal dired-mode-map
  (kbd "M-RET") 'dired-display-file
  (kbd "h") 'dired-up-directory
  (kbd "l") 'dired-find-file) ; use dired-find-file instead of dired-open.
#+end_src
* Elfeed
Elfeed is a great RSS feed reader. I'll user elfeed-org to configure elfeed better
** My elfeed-org directory
#+begin_src emacs-lisp
(setq rmh-elfeed-org-files '("~/.config/doom/elfeed.org"))
#+end_src
** Elfeed only show feeds from last one week
#+begin_src emacs-lisp
(after! elfeed
  (setq elfeed-search-filter "@1-week-ago"))
#+end_src
** Automatic feed update
#+begin_src emacs-lisp
;; (add-hook! 'elfeed-search-mode-hook #'elfeed-update)
#+end_src
